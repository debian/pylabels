Source: pylabels
Section: python
Priority: optional
Maintainer: Georges Khaznadar <georgesk@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-setuptools,
 python3-all,
#Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.2
Homepage: https://github.com/bcbnz/pylabels/
Vcs-Browser: https://salsa.debian.org/debian/pylabels
Vcs-Git: https://salsa.debian.org/debian/pylabels.git

Package: python3-pylabels
Architecture: all
Multi-Arch: foreign
Depends:
 ${python3:Depends},
 ${misc:Depends},
 python3-reportlab,
Description: python library for creating PDFs to print sheets of labels
 pylabels uses the ReportLab PDF toolkit to produce the PDF.
 .
 Basically, the user creates a set of specifications of the label
 sizes etc, writes a callback function which does the actual drawing,
 and gives these two items to a Sheet object. Items are then added to
 the sheet using the add_label() method (or add_labels() to add all
 items from an iterable).
 .
 The callback function is called once for each item, being given a
 ReportLab Drawing object representing the label, its width and
 height, and the item to draw on the label. Any of the standard
 ReportLab drawing methods can be used, with pylabels automatically
 adding a clipping path around each label to prevent it interfering
 with other labels.
 .
 Once all the items have been added, the labels can be saved as a PDF,
 or a preview of a page can be saved as an image.
 .
 This package installs the library for Python 3.
